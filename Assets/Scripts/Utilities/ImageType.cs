namespace Utilities {
	public enum ImageType {
		PNG,
		JPG,
		EXR,
		TGA
	}
}