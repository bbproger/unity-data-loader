using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace Utilities {
	public static class Extensions {
		public static Texture2D ToTexture2D(this Texture texture, TextureFormat textureFormat = TextureFormat.RGB24,
			bool mipChain = false, bool linear = false) {
			return Texture2D.CreateExternalTexture(
				texture.width,
				texture.height,
				textureFormat,
				mipChain,
				linear,
				texture.GetNativeTexturePtr());
		}

		public static byte[] EncodeToType(this Texture2D texture, ImageType imageType) {
			switch (imageType) {
				case ImageType.PNG:
					return texture.EncodeToPNG();
				case ImageType.JPG:
					return texture.EncodeToJPG();
				case ImageType.EXR:
					return texture.EncodeToEXR();
				case ImageType.TGA:
					return texture.EncodeToTGA();
				default:
					return null;
			}
		}

		public static async Task SaveFileAsync(this string data, string path) {
			using (var sw = new StreamWriter(path)) {
				await sw.WriteAsync(data);
			}
		}

		public static async Task<string> LoadFileAsync(this string path) {
			string data = null;
			using (var sr = new StreamReader(path)) {
				data = await sr.ReadToEndAsync();
			}

			return data;
		}
	}
}