using Core;
using Core.Storage;
using UnityEngine;
using Utilities;

namespace Test {
	public class TestStorage : MonoBehaviour {
		class People {
			public string name;
			public int age;

			public People(string name, int age) {
				this.name = name;
				this.age = age;
			}

			public People() {
			}

			public override string ToString() {
				return $"{nameof(name)}: {name}, {nameof(age)}: {age}";
			}
		}

		[SerializeField] private Texture2D texture2D;
		[SerializeField] private SpriteRenderer spriteRenderer;

		private string textAsset = "Hello My name is Arman";
		private People people = new People("Arman", 21);

		private AbstractStorage<string> textAssetStorage =
			new TextAssetStorage("/Users/armankarapetyan/Desktop/testA");

		private AbstractStorage<People> peopleStorage =
			new TypeStorage<People>("/Users/armankarapetyan/Desktop/testA");

		private AbstractStorage<Texture2D> imageStorage =
			new ImageStorage("/Users/armankarapetyan/Desktop/testA", ImageType.PNG);

		public async void Update() {
			if (Input.GetKeyDown(KeyCode.Q)) {
				textAssetStorage?.Save(textAsset, "TextAsset.txt");
			}

			if (Input.GetKeyDown(KeyCode.W)) {
				peopleStorage?.Save(people, "People.txt");
			}

			if (Input.GetKeyDown(KeyCode.E)) {
				imageStorage?.Save(texture2D, "image");
			}

			if (Input.GetKeyDown(KeyCode.A)) {
				string text =
					await textAssetStorage.Load("TextAsset.txt");
				Debug.LogError($"Loaded Text Asset: {text}");
			}

			if (Input.GetKeyDown(KeyCode.S)) {
				var people =
					await peopleStorage.Load("People.txt");
				Debug.LogError(people);
			}

			if (Input.GetKeyDown(KeyCode.D)) {
				var texture =
					await imageStorage.Load("image");
				spriteRenderer.sprite =
					Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), Vector2.one * .5f);
			}
		}
	}
}