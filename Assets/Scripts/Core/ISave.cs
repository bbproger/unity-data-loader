using System.Threading.Tasks;

namespace Core {
	public interface ISave<in T> {
		Task Save(T data, string fileName);
	}
}