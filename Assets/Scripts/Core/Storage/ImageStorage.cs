using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using Utilities;

namespace Core.Storage {
	public class ImageStorage : AbstractStorage<Texture2D> {
		private ImageType imageType;

		public ImageStorage(string basePath, ImageType imageType) : base(basePath) {
			this.imageType = imageType;
		}

		public override async Task Save(Texture2D data, string fileName) {
			var imageExtension = imageType.ToString().ToLower();
			var encodedImage = data.EncodeToType(imageType);
			var path = Path.Combine(BasePath, $"{fileName}.{imageExtension}");
			using (var stream =
				new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None, 4096, true)) {
				stream.Seek(0, SeekOrigin.End);
				await stream.WriteAsync(encodedImage, 0, encodedImage.Length);
			}
		}

		public override async Task<Texture2D> Load(string fileName) {
			var imageExtension = imageType.ToString().ToLower();
			var path = Path.Combine(BasePath, $"{fileName}.{imageExtension}");
			byte[] result;

			using (var stream = File.Open(path, FileMode.Open)) {
				result = new byte[stream.Length];
				await stream.ReadAsync(result, 0, (int) stream.Length);
			}

			var loadTexture = Texture2D.blackTexture;
			return loadTexture.LoadImage(result) ? loadTexture : null;
		}
	}
}