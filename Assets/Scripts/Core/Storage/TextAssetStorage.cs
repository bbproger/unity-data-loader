using System.IO;
using System.Threading.Tasks;
using Utilities;

namespace Core.Storage {
	public class TextAssetStorage : AbstractStorage<string> {
		public TextAssetStorage(string basePath) : base(basePath) {
		}

		public override async Task Save(string data, string fileName) {
			var path = Path.Combine(BasePath, fileName);
			await data.SaveFileAsync(path);
		}

		public override async Task<string> Load(string fileName) {
			var path = Path.Combine(BasePath, fileName);
			var text = await path.LoadFileAsync();
			return text;
		}
	}
}