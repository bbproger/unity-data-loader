using System.Threading.Tasks;

namespace Core.Storage {
	public abstract class AbstractStorage<T> : ISave<T>, ILoad<T> {
		protected string BasePath { get; private set; }

		protected AbstractStorage(string basePath) {
			BasePath = basePath;
		}

		public virtual void SetPath(string path) {
			BasePath = path;
		}

		public abstract Task Save(T data, string fileName);
		public abstract Task<T> Load(string fileName);
	}
}