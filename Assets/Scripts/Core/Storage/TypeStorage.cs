using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using Utilities;

namespace Core.Storage {
	public class TypeStorage<T> : AbstractStorage<T> {
		public TypeStorage(string basePath) : base(basePath) {
		}

		public override async Task Save(T data, string fileName) {
			var path = Path.Combine(BasePath, fileName);
			var encoded = JsonUtility.ToJson(data);
			await encoded.SaveFileAsync(path);
		}

		public override async Task<T> Load(string fileName) {
			var path = Path.Combine(BasePath, fileName);
			var data = await path.LoadFileAsync();
			var decoded = JsonUtility.FromJson<T>(data);
			return decoded;
		}
	}
}