using System.Threading.Tasks;

namespace Core {
	public interface ILoad<T> {
		Task<T> Load(string fileName);
	}
}